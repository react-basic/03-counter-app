import "@testing-library/jest-dom";

import { getSaludo } from "../../bases/02-template-string";


describe('Pruebas en 02-template-string.js', () => { 

    test('getSaludo debe de retornar hola christian', () => { 
        
        const nombre = 'Christian';

        const saludo = getSaludo(nombre);

        expect(saludo).toBe('Hola ' + nombre);
     })

     test('getSaludo debe de retornar Hola Carlos si no hay argumentos', () => { 
         const saludo = getSaludo();
         expect(saludo).toBe('Hola Carlos');
      })

 })