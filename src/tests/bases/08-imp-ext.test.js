import { getHeroeById, getHeroesByOwner } from "../../bases/08-imp-ext";
import heroes from "../../data/heroes";

describe('Pruebas en funciones de Heroes', () => { 

    test('Debe de retornar un heroe por id', () => { 

        const id = 1;
        const heroe = getHeroeById(id);

        const heroeData = heroes.find(heroe => heroe.id === id);

        expect(heroe).toEqual(heroeData);
     })

     test('Debe de retornar undefined si heroe no existe', () => { 

        const id = 10;
        const heroe = getHeroeById(id);

        expect(heroe).toBe(undefined);
     })

     test('Debe de retornar un heroe DC', () => { 

        const owner = 'DC';
        const heroes = getHeroesByOwner(owner);

        const heroeData = heroes.filter(heroe => heroe.owner === owner);

        expect(heroes).toEqual(heroeData);
     })

     test('Debe de retornar un heroe Marvel', () => { 

        const owner = 'Marvel';
        const heroes = getHeroesByOwner(owner);

        expect(heroes.length).toBe(2);
     })

 })