import { getUser, getUsuarioActivo } from "../../bases/05-funciones";

describe('Pruebas en 05-funciones.js', () => { 

    test('getUser debe retornar un objeto', () => { 

        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi13234'
        };

        const user = getUser();

        // toEqueal analiza cada propiedad del objeto
        expect(user).toEqual(userTest);

     })

     test('getUsuarioActivo debe retornar un objeto', () => { 

        const nombre = 'cdionisio';

        const userTest = {
            uid: 'ABC213',
            username: nombre
        }

        const user = getUsuarioActivo(nombre);

        expect(user).toEqual(userTest);


      })

 })