import { getHeroeByIdAsync } from "../../bases/09-promesas"
import heroes from "../../data/heroes";


describe('Pruebas con promesas', () => { 

    test('getHeroeByIdAsync debe retornar un Heroe async', (done) => { 
        
        const id = 1;
        
        getHeroeByIdAsync(id)
            .then( heroe => {
                expect(heroe).toBe(heroes[0]);
                done();
            });
     })

     test('Debe obtener un error si el heroe por id no existe', () => { 

        const id = 10;

        return expect(getHeroeByIdAsync(id)).rejects.toBe(
            'El heroe no existe'
        );
        

      })

 })