import React from 'react';
import { render, screen } from "@testing-library/react";
import PrimeraApp from "../PrimeraApp";
import { shallow } from "enzyme";

describe('Pruebas en <PrimeraApp />', () => { 

    // test('debe de mostrar el mensaje "Hola Mundo"', () => { 

    //     const saludo = 'Hola Mundo';

    //     render(<PrimeraApp saludo={saludo}/>);

    //     expect(screen.getByText(saludo)).toBeInTheDocument();
    //  })

    test('debe mostrar <PrimeraApp /> correctamente', () => { 

        const saludo = 'Hola, Soy Goku';
        const wrapper = shallow(<PrimeraApp saludo={saludo} />);

        expect(wrapper).toMatchSnapshot();
     })

     test('debe mostrar el subtitulo enviado por props', () => { 
        
        const saludo = 'Hola, Soy Goku';
        const subtitulo = 'Soy el subtitulo';
        const wrapper = shallow(
            <PrimeraApp 
                saludo={saludo} 
                subtitulo={subtitulo}
            />
        );

        const textoParrafo = wrapper.find('p').text();
        console.log(textoParrafo);

        expect(textoParrafo).toBe(subtitulo);
      })

 })