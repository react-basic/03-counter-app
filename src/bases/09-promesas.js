import { getHeroeById } from "./08-imp-ext";


export const getHeroeByIdAsync = (id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const heroe = getHeroeById(id);

            if (heroe === undefined) {
                reject('El heroe no existe');
            } else {
                resolve(heroe)
            } 
            // reject('No se pudo encotrar el héroe');
        }, 1500);
    });
}