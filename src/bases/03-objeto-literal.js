

const persona = {
    nombre: 'Tony',
    apellido: 'Stark',
    edad: 45,
    direccion: {
        ciudad: 'New York',
        zip: 555546,
        lat: 14.45646,
        lng: 34.54684,
    }
};

// console.table(persona);


const persona2 = {...persona};
persona2.nombre = 'Peter';

console.log(persona);
console.log(persona2);